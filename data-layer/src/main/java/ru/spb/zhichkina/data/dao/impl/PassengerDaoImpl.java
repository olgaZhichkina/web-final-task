package ru.spb.zhichkina.data.dao.impl;

import org.apache.log4j.LogMF;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import ru.spb.zhichkina.data.dao.PassengerDao;
import ru.spb.zhichkina.data.entities.Passenger;
import ru.spb.zhichkina.data.entities.Ticket;
import ru.spb.zhichkina.data.entities.Train;
import ru.spb.zhichkina.data.entities.Trip;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @author Olga Zhichkina
 */
@Component
public class PassengerDaoImpl implements PassengerDao {
    private Logger log = Logger.getLogger(PassengerDaoImpl.class);

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    @Transactional
    public Boolean addPassenger(String firstName, String lastName, Date birthDate, String login, String password) {

        //check if already registered
        List<String> lastNames = entityManager.createQuery("select p.lastName from Passenger p").getResultList();
        if (lastNames.contains(lastName)) {
            List<String> firstNames = entityManager.createQuery("select p.firstName from Passenger p where p.lastName = :ln")
                    .setParameter("ln", lastName)
                    .getResultList();
            if (firstNames.contains(firstName)) {
                Date birth = (Date) entityManager.createQuery("select p.dateOfBirth from Passenger p where p.lastName = :ln and p.firstName = :fn")
                        .setParameter("ln", lastName)
                        .setParameter("fn", firstName)
                        .getSingleResult();
                if (birthDate.equals(birth)) {
                    LogMF.trace(log, "Passenger {0} {1} has already been registered", firstName, lastName);
                    return false;
                }
            }
        }

        //check if the login is already in use
        List<String> userNames = entityManager.createQuery("select p.login from Passenger p").getResultList();
        if (userNames.contains(login)) {
            LogMF.trace(log, "Login {0} is already in use and could not be registered", login);
            return false;
        }

        Passenger passenger = new Passenger();
        passenger.setFirstName(firstName);
        passenger.setLastName(lastName);
        passenger.setDateOfBirth(birthDate);
        passenger.setLogin(login);
        passenger.setPassword(password);
        passenger.setRole("ROLE_USER");
        entityManager.persist(passenger);
        LogMF.trace(log, "New passenger with login \"{0}\" has been registered", login);
        log.trace("-addPassenger");
        return true;
    }

    @Override
    @Transactional(readOnly = true)
    public List<Passenger> getPassengersByTrain(String trainName, Date date) {
        List<Passenger> passengers = new ArrayList<>();
        Train train = (Train) entityManager.createQuery("from Train t where t.name = :name")
                .setParameter("name", trainName)
                .getSingleResult();
        try {
            Trip trip = (Trip) entityManager.createQuery("from Trip t where t.train = :train AND t.date = :date")
                    .setParameter("train", train)
                    .setParameter("date", date)
                    .getSingleResult();
            List<Ticket> tickets = entityManager.createQuery("from Ticket t where t.trip = :trip")
                    .setParameter("trip", trip)
                    .getResultList();
            for (Ticket t : tickets) {
                passengers.add(t.getPassenger());
            }
        }catch (NoResultException e) {
            LogMF.error(log, "No trips were created for the specified date: {0}", new Object[] {date});
        }
        return passengers;
    }
}
