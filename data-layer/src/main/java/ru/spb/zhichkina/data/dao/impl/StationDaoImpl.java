package ru.spb.zhichkina.data.dao.impl;

import org.apache.log4j.LogMF;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import ru.spb.zhichkina.data.dao.StationDao;
import ru.spb.zhichkina.data.entities.Station;
import ru.spb.zhichkina.data.entities.TimeTableLine;
import ru.spb.zhichkina.data.entities.Train;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

/**
 * @author Olga Zhichkina
 */
@Component
public class StationDaoImpl implements StationDao {
    private Logger log = Logger.getLogger(StationDaoImpl.class);

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    @Transactional(readOnly = true)
    public List<Station> getAllStations() {
        return entityManager.createQuery("from Station s").getResultList();
    }

    @Override
    @Transactional(readOnly = true)
    public String getStationNameById(Long id) {
        return (String) entityManager.createQuery("select s.name from Station s where s.id = :id")
                .setParameter("id", id)
                .getSingleResult();
    }

    @Override
    @Transactional
    public Boolean addStation(String name) {

        //check if already exists
        List<String> stationNames = entityManager.createQuery("select s.name from Station s").getResultList();
        if (stationNames.contains(name)) {
            LogMF.trace(log, "Station with the name {0} already exists", name);
            return false;
        }

        Station station = new Station();
        station.setName(name);
        entityManager.persist(station);
        LogMF.trace(log, "New station \"{0}\" has been added", name);
        return true;
    }

    @Override
    @Transactional(readOnly = true)
    public List<TimeTableLine> getStationsByTrain(String trainName) {
        Train train = (Train) entityManager.createQuery("from Train t where t.name = :name")
                .setParameter("name", trainName)
                .getSingleResult();
        return train.getLines();
    }
}
