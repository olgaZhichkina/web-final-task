package ru.spb.zhichkina.data.utils;

import java.text.SimpleDateFormat;

/**
 * @author Olga Zhichkina
 */
public class DateTimeUtils {
    public static final SimpleDateFormat TIME_FORMATTER = new SimpleDateFormat("HH:mm");
    public static final SimpleDateFormat DATE_FORMATTER = new SimpleDateFormat("dd.MM.yyyy");
    public static final SimpleDateFormat DATE_PARSER = new SimpleDateFormat("yyyy-MM-dd");
    public static final int NUMBER_OF_TRIPS_PER_TRAIN = 7;
}
