package ru.spb.zhichkina.data.dao.impl;

import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;
import org.apache.log4j.LogMF;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import ru.spb.zhichkina.data.dao.TrainDao;
import ru.spb.zhichkina.data.entities.Station;
import ru.spb.zhichkina.data.entities.TimeTableLine;
import ru.spb.zhichkina.data.entities.Train;
import ru.spb.zhichkina.data.entities.Trip;
import ru.spb.zhichkina.data.utils.DateTimeUtils;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * @author Olga Zhichkina
 */
@Component
public class TrainDaoImpl implements TrainDao {
    private Logger log = Logger.getLogger(TrainDaoImpl.class);

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    @Transactional(readOnly = true)
    public List<Pair<TimeTableLine, TimeTableLine>> getTrainsByRoute(Long departureStationId, Long arrivalStationId) {
        List<Pair<TimeTableLine, TimeTableLine>> result = new ArrayList<>();
        List<TimeTableLine> lines = entityManager.createQuery("from TimeTableLine tl where tl.station.id = :stationId")
                .setParameter("stationId", departureStationId)
                .getResultList();
        for (TimeTableLine line : lines) {
            TimeTableLine arrival = null;
            List list = entityManager.createQuery("from TimeTableLine tl where tl.station.id = :stationId and tl.train = :train and tl.time > :time")
                    .setParameter("stationId", arrivalStationId)
                    .setParameter("train", line.getTrain())
                    .setParameter("time", line.getTime())
                    .getResultList();
            if (list.size() > 0)
                arrival = (TimeTableLine) list.get(0);
            if (arrival != null) {
                result.add(new ImmutablePair<>(line, arrival));
            }
        }
        return result;
    }

    @Override
    @Transactional(readOnly = true)
    public List<TimeTableLine> getTrainsByStation(String stationName) {
        Long stationId = getStationIdByName(stationName);
        return entityManager.createQuery("from TimeTableLine tl where tl.station.id = :stationId")
                .setParameter("stationId", stationId)
                .getResultList();
    }

    @Override
    @Transactional
    public Boolean addTrain(String name, int numberOfSeats, String[] stationNames, Date[] times) {

        //check if already exists
        List<String> trainNames = entityManager.createQuery("select t.name from Train t").getResultList();
        if (trainNames.contains(name)) {
            LogMF.trace(log, "Train with the name {0} already exists", name);
            return false;
        }

        Train train = new Train();
        train.setName(name);
        train.setNumberOfSeats(numberOfSeats);
        entityManager.persist(train);
        List<TimeTableLine> lines = new ArrayList<>();
        for (int i = 0; i < stationNames.length; i++) {
            TimeTableLine line = new TimeTableLine();
            Station station = entityManager.find(Station.class, getStationIdByName(stationNames[i]));
            line.setStation(station);
            line.setTime(times[i]);
            line.setTrain(train);
            lines.add(line);
            entityManager.persist(line);
        }
        train.setLines(lines);
        LogMF.trace(log, "New train \"{0}\" has been added", name);
        List<Trip> trips = new ArrayList<>();
        for (int i = 0; i < DateTimeUtils.NUMBER_OF_TRIPS_PER_TRAIN; i++) {
            Trip trip = new Trip();
            trip.setTrain(train);
            trip.setNumberOfFreeSeats(numberOfSeats);
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(new Date());
            calendar.add(Calendar.DAY_OF_YEAR, i);
            trip.setDate(calendar.getTime());
            trips.add(trip);
            entityManager.persist(trip);
        }
        train.setTrips(trips);
        LogMF.trace(log, "Trips for train \"{0}\" have been added", name);
        return true;
    }

    @Override
    @Transactional(readOnly = true)
    public List<Train> getAllTrains() {
        return entityManager.createQuery("from Train t")
                .getResultList();
    }

    @Override
    @Transactional(readOnly = true)
    public String getTrainNameById(Long id) {
        return (String) entityManager.createQuery("select t.name from Train t where t.id = :id")
                .setParameter("id", id)
                .getSingleResult();
    }

    @Transactional(readOnly = true)
    private Long getStationIdByName(String name) {
        return (Long) entityManager.createQuery("select s.id from Station s where s.name = :name")
                .setParameter("name", name)
                .getSingleResult();
    }
}
