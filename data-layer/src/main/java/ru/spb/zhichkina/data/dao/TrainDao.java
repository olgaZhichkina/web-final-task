package ru.spb.zhichkina.data.dao;

import org.apache.commons.lang3.tuple.Pair;
import ru.spb.zhichkina.data.entities.TimeTableLine;
import ru.spb.zhichkina.data.entities.Train;

import java.util.Date;
import java.util.List;

/**
 * @author Olga Zhichkina
 */
public interface TrainDao {
    List<Pair<TimeTableLine, TimeTableLine>> getTrainsByRoute(Long departureStationId, Long arrivalStationId);

    List<TimeTableLine> getTrainsByStation(String stationName);

    Boolean addTrain(String name, int numberOfSeats, String[] stationNames, Date[] times);

    List<Train> getAllTrains();

    String getTrainNameById(Long id);
}
