package ru.spb.zhichkina.data.dao;

import java.util.Date;

/**
 * @author Olga Zhichkina
 */
public interface TripDao {
    Integer getFreeSeatsByTrain(String trainName, Date date);
}
