package ru.spb.zhichkina.data.dao;

import ru.spb.zhichkina.data.entities.Passenger;

import java.util.Date;
import java.util.List;

/**
 * @author Olga Zhichkina
 */
public interface PassengerDao {
    Boolean addPassenger(String firstName, String lastName, Date birthDate, String login, String password);

    List<Passenger> getPassengersByTrain(String trainName, Date date);
}
