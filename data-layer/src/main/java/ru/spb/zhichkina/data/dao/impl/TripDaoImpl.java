package ru.spb.zhichkina.data.dao.impl;

import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import ru.spb.zhichkina.data.dao.TripDao;
import ru.spb.zhichkina.data.entities.Train;
import ru.spb.zhichkina.data.entities.Trip;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.Date;
import java.util.List;

/**
 * @author Olga Zhichkina
 */
@Component
public class TripDaoImpl implements TripDao {

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    @Transactional(readOnly = true)
    public Integer getFreeSeatsByTrain(String trainName, Date date) {
        Train train = (Train) entityManager.createQuery("from Train t where t.name = :name")
                .setParameter("name", trainName)
                .getSingleResult();
        List<Trip> trips = entityManager.createQuery("from Trip t where t.train = :train and t.date = :date")
                .setParameter("train", train)
                .setParameter("date", date)
                .getResultList();
        if (trips.size() > 0) {
            return trips.get(0).getNumberOfFreeSeats();
        }
        return 0;
    }
}
