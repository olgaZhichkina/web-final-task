package ru.spb.zhichkina.data.entities;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

/**
 * @author Olga Zhichkina
 */
@Entity
@Table(name = "stations")
public class Station implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    @Column(name = "id", unique = true, nullable = false)
    private long id;

    @Column(name = "name")
    private String name;

    @ManyToMany(mappedBy = "stations")
    private List<Train> trains;

    @OneToMany(mappedBy = "station", fetch = FetchType.LAZY)
    private List<TimeTableLine> lines;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Train> getTrains() {
        return trains;
    }

    public void setTrains(List<Train> trains) {
        this.trains = trains;
    }

    public List<TimeTableLine> getLines() {
        return lines;
    }

    public void setLines(List<TimeTableLine> lines) {
        this.lines = lines;
    }
}
