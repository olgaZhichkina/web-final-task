package ru.spb.zhichkina.data.dao;

import ru.spb.zhichkina.data.entities.Station;
import ru.spb.zhichkina.data.entities.TimeTableLine;

import java.util.List;

/**
 * @author Olga Zhichkina
 */
public interface StationDao {
    List<Station> getAllStations();

    String getStationNameById(Long id);

    Boolean addStation(String name);

    List<TimeTableLine> getStationsByTrain(String trainName);
}
