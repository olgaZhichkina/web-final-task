package ru.spb.zhichkina.data.dao;

import ru.spb.zhichkina.data.entities.Ticket;

import java.util.Date;
import java.util.List;

/**
 * @author Olga Zhichkina
 */
public interface TicketDao {

    List<Ticket> getMyTickets(String login);

    Boolean buyTicket(long departureId, long arrivalId, String passengerName, Date date);
}
