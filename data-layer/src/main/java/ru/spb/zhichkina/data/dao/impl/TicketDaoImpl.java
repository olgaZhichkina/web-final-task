package ru.spb.zhichkina.data.dao.impl;

import org.apache.log4j.LogMF;
import org.apache.log4j.Logger;
import org.joda.time.DateTime;
import org.joda.time.Minutes;
import org.joda.time.MonthDay;
import org.joda.time.TimeOfDay;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import ru.spb.zhichkina.data.dao.TicketDao;
import ru.spb.zhichkina.data.entities.*;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.Date;
import java.util.List;

/**
 * @author Olga Zhichkina
 */
@Component
public class TicketDaoImpl implements TicketDao {
    private Logger log = Logger.getLogger(TicketDaoImpl.class);

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    @Transactional(readOnly = true)
    public List<Ticket> getMyTickets(String login) {
        return entityManager.createQuery("from Ticket t where t.passenger.login = :login")
                .setParameter("login", login)
                .getResultList();
    }

    @Override
    @Transactional
    public Boolean buyTicket(long departureId, long arrivalId, String login, Date date) {
        TimeTableLine departure = entityManager.find(TimeTableLine.class, departureId);
        Date now = new Date();
        MonthDay dateOfTrip = MonthDay.fromDateFields(date);
        MonthDay dateOfNow = MonthDay.fromDateFields(now);
        DateTime timeOfTrip = new DateTime(departure.getTime());
        DateTime timeOfNow = new DateTime(now);

        //check date and time
        if (dateOfTrip.compareTo(dateOfNow) < 0) {
            return false;
        } else if (dateOfTrip.compareTo(dateOfNow) == 0){
            if (Minutes.minutesBetween(timeOfNow, timeOfTrip).getMinutes() < 10) {
                return false;
            }
        }

        Train train = departure.getTrain();
        Trip trip = (Trip) entityManager.createQuery("from Trip t where t.train = :train AND t.date = :date")
                .setParameter("train", train)
                .setParameter("date", date)
                .getSingleResult();

        //check seats availability
        if (trip.getNumberOfFreeSeats() < 1) {
            LogMF.trace(log, "No seats available for trip {0}", trip.getId());
            return false;
        }

        Passenger passenger = (Passenger) entityManager.createQuery("from Passenger p where p.login = :login")
                .setParameter("login", login)
                .getSingleResult();

        //check if already bought
        List<Ticket> tickets = entityManager.createQuery("from Ticket t where t.passenger = :passenger AND t.trip = :trip")
                .setParameter("passenger", passenger)
                .setParameter("trip", trip)
                .getResultList();
        if (tickets.size() != 0) {
            LogMF.trace(log, "The ticket for the train {0} on {1} has already been bought by the passenger {2} {3}",
                    train.getName(), date, passenger.getFirstName(), passenger.getLastName());
            return false;
        }

        TimeTableLine arrival = entityManager.find(TimeTableLine.class, arrivalId);
        Ticket ticket = new Ticket();
        ticket.setDateOfPurchase(new Date());
        ticket.setPassenger(passenger);
        ticket.setDeparture(departure);
        ticket.setArrival(arrival);
        ticket.setTrip(trip);
        entityManager.persist(ticket);
        trip.setNumberOfFreeSeats(trip.getNumberOfFreeSeats() - 1);
        log.trace("The ticket has been successfully bought");
        return true;
    }
}
