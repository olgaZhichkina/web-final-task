package ru.spb.zhichkina.data.entities;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

/**
 * @author Olga Zhichkina
 */
@Entity
@Table(name = "tickets")
public class Ticket implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    @Column(name = "id", unique = true, nullable = false)
    private long id;

    @Temporal(TemporalType.DATE)
    @Column(name = "date_of_purchase")
    private Date dateOfPurchase;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "departure_id")
    private TimeTableLine departure;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "arrival_id")
    private TimeTableLine arrival;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "passenger_id")
    private Passenger passenger;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "trip_id")
    private Trip trip;

/*    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "train_id")
    private Train train;

    @Temporal(TemporalType.DATE)
    @Column(name = "date_of_trip")
    private Date dateOfTrip;*/

    public TimeTableLine getDeparture() {
        return departure;
    }

    public void setDeparture(TimeTableLine arrivalId) {
        this.departure = arrivalId;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Date getDateOfPurchase() {
        return dateOfPurchase;
    }

    public TimeTableLine getArrival() {
        return arrival;
    }

    public void setArrival(TimeTableLine arrival) {
        this.arrival = arrival;
    }

    public void setDateOfPurchase(Date dateOfPurchase) {
        this.dateOfPurchase = dateOfPurchase;
    }

    public Passenger getPassenger() {
        return passenger;
    }

    public void setPassenger(Passenger passenger) {
        this.passenger = passenger;
    }

    public Trip getTrip() {
        return trip;
    }

    public void setTrip(Trip trip) {
        this.trip = trip;
    }
}