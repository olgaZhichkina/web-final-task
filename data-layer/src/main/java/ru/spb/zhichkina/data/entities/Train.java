package ru.spb.zhichkina.data.entities;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

/**
 * @author Olga Zhichkina
 */
@Entity
@Table(name = "trains")
public class Train implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    @Column(name = "id", unique = true, nullable = false)
    private long id;

    @Column(name = "number_of_seats")
    private int numberOfSeats;

    @Column(name = "name")
    private String name;

    @ManyToMany(cascade = CascadeType.ALL)
    @JoinTable(name = "time_table", joinColumns = {@JoinColumn(name = "train_id")}, inverseJoinColumns = {@JoinColumn(name = "station_id")})
    private List<Station> stations;

    @OneToMany(mappedBy = "train", fetch = FetchType.LAZY)
    private List<TimeTableLine> lines;

    @OneToMany(mappedBy = "train", fetch = FetchType.LAZY)
    private List<Trip> trips;

/*    @OneToMany(mappedBy = "train", fetch = FetchType.LAZY)
    private List<Ticket> tickets;*/

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public int getNumberOfSeats() {
        return numberOfSeats;
    }

    public void setNumberOfSeats(int numberOfSeats) {
        this.numberOfSeats = numberOfSeats;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Station> getStations() {
        return stations;
    }

    public void setStations(List<Station> stations) {
        this.stations = stations;
    }

    public List<TimeTableLine> getLines() {
        return lines;
    }

    public void setLines(List<TimeTableLine> lines) {
        this.lines = lines;
    }

    public List<Trip> getTrips() {
        return trips;
    }

    public void setTrips(List<Trip> trips) {
        this.trips = trips;
    }
}

