INSERT INTO passengers (`first_name`,`last_name`,`date_of_birth`,`username`,`password`,`authority`, `enabled`) VALUES ('Dave','Grohl','1969-1-14','dave','123','ROLE_USER', 1);
INSERT INTO passengers (`first_name`,`last_name`,`date_of_birth`,`username`,`password`,`authority`, `enabled`) VALUES ('Shaun','Morgan','1978-12-21','shaun','qwerty','ROLE_USER', 1);
INSERT INTO passengers (`first_name`,`last_name`,`date_of_birth`,`username`,`password`,`authority`, `enabled`) VALUES ('Brian','Molko','1972-12-10','brian','888','ROLE_USER', 1);
INSERT INTO passengers (`first_name`,`last_name`,`date_of_birth`,`username`,`password`,`authority`, `enabled`) VALUES ('Kurt','Cobain','1967-2-20','kurt','999','ROLE_USER', 1);
INSERT INTO passengers (`first_name`,`last_name`,`date_of_birth`,`username`,`password`,`authority`, `enabled`) VALUES ('Courtney','Love','1964-7-09','admin','admin','ROLE_ADMIN', 1);

INSERT INTO stations (`name`) VALUES ('Berlin'),('Hannover'), ('Frankfurt'), ('Barcelona'), ('Madrid');

INSERT INTO trains (`number_of_seats`,`name`) VALUES (10, 'InterCity');
INSERT INTO trains (`number_of_seats`,`name`) VALUES (5, 'ICE');
INSERT INTO trains (`number_of_seats`,`name`) VALUES (3, 'NightExpress');
INSERT INTO trains (`number_of_seats`,`name`) VALUES (2, 'EuroExpress');

INSERT INTO time_table (`station_id`,`train_id`,`time`) VALUES (1,1,'1970-01-01 12:15:00');
INSERT INTO time_table (`station_id`,`train_id`,`time`) VALUES (2,1,'1970-01-01 14:30:00');
INSERT INTO time_table (`station_id`,`train_id`,`time`) VALUES (1,2,'1970-01-01 14:00:00');
INSERT INTO time_table (`station_id`,`train_id`,`time`) VALUES (2,2,'1970-01-01 16:00:00');
INSERT INTO time_table (`station_id`,`train_id`,`time`) VALUES (3,2,'1970-01-01 20:00:00');
INSERT INTO time_table (`station_id`,`train_id`,`time`) VALUES (1,3,'1970-01-01 19:00:00');
INSERT INTO time_table (`station_id`,`train_id`,`time`) VALUES (2,3,'1970-01-01 20:45:00');
INSERT INTO time_table (`station_id`,`train_id`,`time`) VALUES (3,3,'1970-01-01 23:55:00');
INSERT INTO time_table (`station_id`,`train_id`,`time`) VALUES (3,4,'1970-01-01 10:05:00');
INSERT INTO time_table (`station_id`,`train_id`,`time`) VALUES (4,4,'1970-01-01 16:15:00');
INSERT INTO time_table (`station_id`,`train_id`,`time`) VALUES (5,4,'1970-01-01 17:00:00');

INSERT INTO trips (`train_id`, `number_of_free_seats`,`date`) VALUES (1, 8, '2014-9-27');
INSERT INTO trips (`train_id`, `number_of_free_seats`,`date`) VALUES (1, 9, '2014-9-28');
INSERT INTO trips (`train_id`, `number_of_free_seats`,`date`) VALUES (1, 10, '2014-9-29');
INSERT INTO trips (`train_id`, `number_of_free_seats`,`date`) VALUES (1, 10, '2014-9-30');
INSERT INTO trips (`train_id`, `number_of_free_seats`,`date`) VALUES (1, 10, '2014-10-1');
INSERT INTO trips (`train_id`, `number_of_free_seats`,`date`) VALUES (1, 10, '2014-10-2');
INSERT INTO trips (`train_id`, `number_of_free_seats`,`date`) VALUES (1, 10, '2014-10-3');

INSERT INTO trips (`train_id`, `number_of_free_seats`,`date`) VALUES (2, 4, '2014-9-27');
INSERT INTO trips (`train_id`, `number_of_free_seats`,`date`) VALUES (2, 5, '2014-9-28');
INSERT INTO trips (`train_id`, `number_of_free_seats`,`date`) VALUES (2, 4, '2014-9-29');
INSERT INTO trips (`train_id`, `number_of_free_seats`,`date`) VALUES (2, 5, '2014-9-30');
INSERT INTO trips (`train_id`, `number_of_free_seats`,`date`) VALUES (2, 5, '2014-10-1');
INSERT INTO trips (`train_id`, `number_of_free_seats`,`date`) VALUES (2, 5, '2014-10-2');
INSERT INTO trips (`train_id`, `number_of_free_seats`,`date`) VALUES (2, 5, '2014-10-3');

INSERT INTO trips (`train_id`, `number_of_free_seats`,`date`) VALUES (3, 2, '2014-9-27');
INSERT INTO trips (`train_id`, `number_of_free_seats`,`date`) VALUES (3, 3, '2014-9-28');
INSERT INTO trips (`train_id`, `number_of_free_seats`,`date`) VALUES (3, 3, '2014-9-29');
INSERT INTO trips (`train_id`, `number_of_free_seats`,`date`) VALUES (3, 3, '2014-9-30');
INSERT INTO trips (`train_id`, `number_of_free_seats`,`date`) VALUES (3, 3, '2014-10-1');
INSERT INTO trips (`train_id`, `number_of_free_seats`,`date`) VALUES (3, 3, '2014-10-2');
INSERT INTO trips (`train_id`, `number_of_free_seats`,`date`) VALUES (3, 3, '2014-10-3');

INSERT INTO trips (`train_id`, `number_of_free_seats`,`date`) VALUES (4, 1, '2014-9-27');
INSERT INTO trips (`train_id`, `number_of_free_seats`,`date`) VALUES (4, 1, '2014-9-28');
INSERT INTO trips (`train_id`, `number_of_free_seats`,`date`) VALUES (4, 2, '2014-9-29');
INSERT INTO trips (`train_id`, `number_of_free_seats`,`date`) VALUES (4, 1, '2014-9-30');
INSERT INTO trips (`train_id`, `number_of_free_seats`,`date`) VALUES (4, 1, '2014-10-1');
INSERT INTO trips (`train_id`, `number_of_free_seats`,`date`) VALUES (4, 2, '2014-10-2');
INSERT INTO trips (`train_id`, `number_of_free_seats`,`date`) VALUES (4, 2, '2014-10-3');

INSERT INTO tickets (`passenger_id`,`trip_id`, `date_of_purchase`,`departure_id`, `arrival_id`) VALUES (5,1,'2014-9-20', 1, 2);
INSERT INTO tickets (`passenger_id`,`trip_id`, `date_of_purchase`,`departure_id`, `arrival_id`) VALUES (5,22,'2014-9-20', 9, 11);
INSERT INTO tickets (`passenger_id`,`trip_id`, `date_of_purchase`,`departure_id`, `arrival_id`) VALUES (4,8,'2014-9-20', 3, 5);
INSERT INTO tickets (`passenger_id`,`trip_id`, `date_of_purchase`,`departure_id`, `arrival_id`) VALUES (4,15,'2014-9-20', 6, 8);
INSERT INTO tickets (`passenger_id`,`trip_id`, `date_of_purchase`,`departure_id`, `arrival_id`) VALUES (4,23,'2014-9-21', 9, 11);
INSERT INTO tickets (`passenger_id`,`trip_id`, `date_of_purchase`,`departure_id`, `arrival_id`) VALUES (3,10,'2014-9-22', 4, 5);
INSERT INTO tickets (`passenger_id`,`trip_id`, `date_of_purchase`,`departure_id`, `arrival_id`) VALUES (3,25,'2014-9-23', 9, 11);
INSERT INTO tickets (`passenger_id`,`trip_id`, `date_of_purchase`,`departure_id`, `arrival_id`) VALUES (2,1,'2014-9-20', 1, 2);
INSERT INTO tickets (`passenger_id`,`trip_id`, `date_of_purchase`,`departure_id`, `arrival_id`) VALUES (1,2,'2014-9-21', 1, 2);
INSERT INTO tickets (`passenger_id`,`trip_id`, `date_of_purchase`,`departure_id`, `arrival_id`) VALUES (1,26,'2014-9-24', 9, 10);