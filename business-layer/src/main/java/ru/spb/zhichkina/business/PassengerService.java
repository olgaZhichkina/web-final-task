package ru.spb.zhichkina.business;

import org.apache.commons.lang3.tuple.Pair;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.spb.zhichkina.beans.LineBean;
import ru.spb.zhichkina.beans.TicketBean;
import ru.spb.zhichkina.data.dao.*;
import ru.spb.zhichkina.data.entities.Station;
import ru.spb.zhichkina.data.entities.Ticket;
import ru.spb.zhichkina.data.entities.TimeTableLine;
import ru.spb.zhichkina.data.utils.DateTimeUtils;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @author Olga Zhichkina
 */
@Component
public class PassengerService {
    @Autowired
    private PassengerDao passengerDao;

    @Autowired
    private StationDao stationDao;

    @Autowired
    private TicketDao ticketDao;

    @Autowired
    private TrainDao trainDao;

    @Autowired
    private TripDao tripDao;

    public List<Station> getStations() {
        return stationDao.getAllStations();
    }

    public List<LineBean> getTrainsByRoute(String departureStationId, String arrivalStationId, String dateString) {
        List<Pair<TimeTableLine, TimeTableLine>> result = trainDao.getTrainsByRoute(Long.parseLong(departureStationId),
                Long.parseLong(arrivalStationId));
        List<LineBean> lines = new ArrayList<>();
        for (Pair<TimeTableLine, TimeTableLine> pair : result) {
            TimeTableLine departure = pair.getLeft();
            TimeTableLine arrival = pair.getRight();
            String trainName = departure.getTrain().getName();
            Date date = null;
            try {
                date = DateTimeUtils.DATE_PARSER.parse(dateString);
            } catch (ParseException e) {
                e.printStackTrace();
            }
            LineBean line = new LineBean();
            line.setTrain(trainName);
            line.setDepartureStation(departure.getStation().getName());
            line.setTimeOfDeparture(DateTimeUtils.TIME_FORMATTER.format(departure.getTime()));
            line.setArrivalStation(arrival.getStation().getName());
            line.setTimeOfArrival(DateTimeUtils.TIME_FORMATTER.format(arrival.getTime()));
            line.setNumberOfSeats(tripDao.getFreeSeatsByTrain(trainName, date).toString());
            line.setDepartureId(Long.toString(departure.getId()));
            line.setArrivalId(Long.toString(arrival.getId()));
            lines.add(line);
        }
        return lines;
    }

    public List<LineBean> getTrainsByStation(String stationId) {
        Long id = Long.parseLong(stationId);
        String stationName = stationDao.getStationNameById(id);
        List<TimeTableLine> result = trainDao.getTrainsByStation(stationName);
        List<LineBean> lines = new ArrayList<>();
        for (TimeTableLine timeTableLine : result) {
            LineBean line = new LineBean();
            line.setTrain(timeTableLine.getTrain().getName());
            line.setDepartureStation(timeTableLine.getStation().getName());
            line.setTimeOfDeparture(DateTimeUtils.TIME_FORMATTER.format(timeTableLine.getTime()));
            line.setArrivalStation("");
            line.setTimeOfArrival("");
            line.setNumberOfSeats("");
            line.setDepartureId("");
            line.setArrivalId("");
            lines.add(line);
        }
        return lines;
    }

    public List<TicketBean> getMyTickets(String login) {
        List<Ticket> result = ticketDao.getMyTickets(login);
        List<TicketBean> tickets = new ArrayList<>();
        for (Ticket ticket : result) {
            TicketBean ticketBean = new TicketBean();
            ticketBean.setTicketNumber("" + ticket.getId());
            ticketBean.setTrainName(ticket.getTrip().getTrain().getName());
            ticketBean.setFromStation(ticket.getDeparture().getStation().getName());
            ticketBean.setToStation(ticket.getArrival().getStation().getName());
            ticketBean.setDepartureTime(DateTimeUtils.TIME_FORMATTER.format(ticket.getDeparture().getTime()));
            ticketBean.setArrivalTime(DateTimeUtils.TIME_FORMATTER.format(ticket.getArrival().getTime()));
            ticketBean.setDateOfTrip(DateTimeUtils.DATE_FORMATTER.format(ticket.getTrip().getDate()));
            ticketBean.setDateOfPurchase(DateTimeUtils.DATE_FORMATTER.format(ticket.getDateOfPurchase()));
            tickets.add(ticketBean);
        }
        return tickets;
    }

    public Boolean purchase(String departureIdString, String arrivalIdString, String username, String dateString) {
        Date date = null;
        try {
            date = DateTimeUtils.DATE_PARSER.parse(dateString);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return ticketDao.buyTicket(Long.parseLong(departureIdString), Long.parseLong(arrivalIdString), username, date);
    }

    public Boolean register(String firstName, String lastName, String birthDateString, String login, String password) {
        Date date = null;
        try {
            date = DateTimeUtils.DATE_PARSER.parse(birthDateString);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return passengerDao.addPassenger(firstName, lastName, date, login, password);
    }
}
