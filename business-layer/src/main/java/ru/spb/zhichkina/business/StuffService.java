package ru.spb.zhichkina.business;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.spb.zhichkina.beans.PassengerBean;
import ru.spb.zhichkina.beans.StationBean;
import ru.spb.zhichkina.data.dao.PassengerDao;
import ru.spb.zhichkina.data.dao.StationDao;
import ru.spb.zhichkina.data.dao.TrainDao;
import ru.spb.zhichkina.data.entities.Passenger;
import ru.spb.zhichkina.data.entities.TimeTableLine;
import ru.spb.zhichkina.data.entities.Train;
import ru.spb.zhichkina.data.utils.DateTimeUtils;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @author Olga Zhichkina
 */
@Component
public class StuffService {

    @Autowired
    private PassengerDao passengerDao;

    @Autowired
    private StationDao stationDao;

    @Autowired
    private TrainDao trainDao;

    public Boolean addStation(String name) {
        return stationDao.addStation(name);
    }

    public List<Train> getTrains() {
        return trainDao.getAllTrains();
    }

    public Boolean addTrain(String name, String numberOfSeats, String[] stationNames, String[] timeStrings) {
        Date[] times = new Date[timeStrings.length];
        Date date = null;
        for (int i = 0; i < timeStrings.length; i++) {
            try {
                date = DateTimeUtils.TIME_FORMATTER.parse(timeStrings[i]);
            } catch (ParseException e) {
                e.printStackTrace();
            }
            times[i] = date;
        }
        return trainDao.addTrain(name, Integer.parseInt(numberOfSeats), stationNames, times);
    }

    public List<StationBean> getStationsByTrain(String trainId) {
        Long id = Long.parseLong(trainId);
        String trainName = trainDao.getTrainNameById(id);
        List<TimeTableLine> result = stationDao.getStationsByTrain(trainName);
        List<StationBean> stations = new ArrayList<>();
        for (TimeTableLine timeTableLine : result) {
            StationBean station = new StationBean();
            station.setName(timeTableLine.getStation().getName());
            station.setTime(DateTimeUtils.TIME_FORMATTER.format(timeTableLine.getTime()));
            stations.add(station);
        }
        return stations;
    }

    public List<PassengerBean> getPassengersByTrain(String trainId, String dateString) {
        Long id = Long.parseLong(trainId);
        String trainName = trainDao.getTrainNameById(id);
        Date date = null;
        try {
            date = DateTimeUtils.DATE_PARSER.parse(dateString);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        List<Passenger> result = passengerDao.getPassengersByTrain(trainName, date);
        List<PassengerBean> passengers = new ArrayList<>();
        for (Passenger passenger : result) {
            PassengerBean passengerBean = new PassengerBean();
            passengerBean.setFirstName(passenger.getFirstName());
            passengerBean.setLastName(passenger.getLastName());
            passengerBean.setDateOfBirth(DateTimeUtils.DATE_FORMATTER.format(passenger.getDateOfBirth()));
            passengers.add(passengerBean);
        }
        return passengers;
    }
}
