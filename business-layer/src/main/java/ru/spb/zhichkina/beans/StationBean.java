package ru.spb.zhichkina.beans;

/**
 * @author Olga Zhichkina
 */
public class StationBean {
    String name;
    String time;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }
}
