package ru.spb.zhichkina.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import ru.spb.zhichkina.beans.LineBean;
import ru.spb.zhichkina.beans.TicketBean;
import ru.spb.zhichkina.business.PassengerService;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * @author Olga Zhichkina
 */
@Controller
public class PassengerController {

    @Autowired
    private PassengerService passengerService;

    @RequestMapping(method = RequestMethod.GET, value = "search")
    public String viewSearchPage(Model model) {
        model.addAttribute("departureStationId", 1);
        model.addAttribute("arrivalStationId", 0);
        model.addAttribute("stations", passengerService.getStations());
        model.addAttribute("date", new SimpleDateFormat("yyyy-MM-dd").format(new Date()));
        return "search";
    }

    @RequestMapping(method = RequestMethod.POST, value = "search")
    public String fillInTable(@RequestParam Map<String, String> params, Model model) {
        String departureStationId = params.get("departure");
        String arrivalStationId = params.get("arrival");
        String calendar = params.get("calendar");
        model.addAttribute("departureStationId", departureStationId);
        model.addAttribute("arrivalStationId", arrivalStationId);
        model.addAttribute("date", calendar);
        model.addAttribute("stations", passengerService.getStations());
        List<LineBean> lines;
        if (arrivalStationId.equals("0")) {
            lines = passengerService.getTrainsByStation(departureStationId);
            model.addAttribute("isInfo", "true");
        } else {
            lines = passengerService.getTrainsByRoute(departureStationId, arrivalStationId, calendar);
        }
        model.addAttribute("lines", lines);
        model.addAttribute("searching", "true");
        return "search";
    }

    @RequestMapping(method = RequestMethod.POST, value = "buy")
    @ResponseBody
    public String purchaseTicket(@RequestParam Map<String, String> params) {
        return passengerService.purchase(params.get("from"), params.get("to"),
                SecurityContextHolder.getContext().getAuthentication().getName(), params.get("calendar")).toString();
    }

    @RequestMapping(method = RequestMethod.GET, value = "mytickets")
    public String viewMyTicketsPage(Model model) {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        List<TicketBean> tickets = passengerService.getMyTickets(authentication.getName());
        model.addAttribute("tickets", tickets);
        return "mytickets";
    }

    @RequestMapping(method = RequestMethod.GET, value = "purchaseinfo")
    public String viewInfoPage(Model model) {
        return "purchaseinfo";
    }
}
