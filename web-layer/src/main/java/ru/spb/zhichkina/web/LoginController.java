package ru.spb.zhichkina.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import ru.spb.zhichkina.business.PassengerService;

import java.util.Map;

/**
 * @author Olga Zhichkina
 */
@Controller
public class LoginController {

    @Autowired
    private PassengerService passengerService;

    @RequestMapping(method = RequestMethod.GET, value = "/login")
    public String login(@RequestParam Map<String, String> params, Model model) {
        if (params.get("error") != null) {
            model.addAttribute("error", "Invalid username or password!");
        } else if (params.get("logout") != null) {
            model.addAttribute("success", "Logged out successfully, login again to continue!");
        }
        return "login";
    }

    @RequestMapping(method = RequestMethod.POST, value = "/login")
    public String register(@RequestParam Map<String, String> params, Model model) {
        if (params.get("register") != null) {
            Boolean isRegistered = passengerService.register(params.get("firstName"), params.get("lastName"),
                    params.get("calendar"), params.get("newLogin"), params.get("newPassword"));
            if (isRegistered) {
                model.addAttribute("success", "You are successfully registered, login to continue!");
            } else {
                model.addAttribute("error", "Failed to register you. Please, make sure you are not registered yet" +
                        " and if so, please, choose another login");
                model.addAttribute("firstName", params.get("firstName"));
                model.addAttribute("lastName", params.get("lastName"));
                model.addAttribute("calendar", params.get("calendar"));
                return "registration";
            }
        }
        return "login";
    }

    @RequestMapping(method = RequestMethod.GET, value = "/registration")
    public String viewRegistrationPage(Model model) {
        return "registration";
    }

}
