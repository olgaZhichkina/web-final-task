package ru.spb.zhichkina.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import ru.spb.zhichkina.beans.PassengerBean;
import ru.spb.zhichkina.beans.StationBean;
import ru.spb.zhichkina.business.PassengerService;
import ru.spb.zhichkina.business.StuffService;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * @author Olga Zhichkina
 */
@Controller
public class StuffController {

    @Autowired
    private PassengerService passengerService;

    @Autowired
    private StuffService stuffService;

    @RequestMapping(method = RequestMethod.GET, value = "stations")
    public String viewStationsPage(Model model) {
        model.addAttribute("stations", passengerService.getStations());
        return "stations";
    }

    @RequestMapping(method = RequestMethod.POST, value = "stations")
    public String addStation(@RequestParam Map<String, String> params, Model model) {
        Boolean isAdded = stuffService.addStation(params.get("stationName"));
        if (isAdded) {
            model.addAttribute("success", "The station has been successfully added!");
        } else {
            model.addAttribute("error", "Failed to add a new station. Please, make sure that a station with the same name does not exist");
        }
        model.addAttribute("stations", passengerService.getStations());
        return "stations";
    }

    @RequestMapping(method = RequestMethod.GET, value = "trains")
    public String viewTrainsPage(Model model) {
        model.addAttribute("trains", stuffService.getTrains());
        model.addAttribute("stations", passengerService.getStations());
        return "trains";
    }

    @RequestMapping(method = RequestMethod.POST, value = "trains")
    public String addTrain(@RequestParam Map<String, String> params, Model model) {
        String[] stationNames = new String[2]; //TODO
        stationNames[0] = params.get("stationOnRoute01");
        stationNames[1] = params.get("stationOnRoute02");
        String[] times = new String[2]; //TODO
        times[0] = params.get("time01");
        times[1] = params.get("time02");
        Boolean isAdded = stuffService.addTrain(params.get("trainName"), params.get("numOfSeats"), stationNames, times);
        if (isAdded) {
            model.addAttribute("success", "The train has been successfully added!");
        } else {
            model.addAttribute("error", "Failed to add a new train. Please, make sure that a train with the same name does not exist");
        }
        model.addAttribute("trains", stuffService.getTrains());
        model.addAttribute("stations", passengerService.getStations());
        return "trains";
    }

    @RequestMapping(method = RequestMethod.GET, value = "passengers")
    public String viewPassengersPage(Model model) {
        model.addAttribute("trains", stuffService.getTrains());
        model.addAttribute("date", new SimpleDateFormat("yyyy-MM-dd").format(new Date()));
        return "passengers";
    }

    @RequestMapping(method = RequestMethod.POST, value = "passengers")
    public String showTrainInformation(@RequestParam Map<String, String> params, Model model) {
        String trainId = params.get("train");
        String calendar = params.get("calendar");
        model.addAttribute("trainId", trainId);
        model.addAttribute("date", calendar);
        model.addAttribute("trains", stuffService.getTrains());
        List<StationBean> stations = stuffService.getStationsByTrain(trainId);
        List<PassengerBean> passengers = stuffService.getPassengersByTrain(trainId, calendar);
        model.addAttribute("stations", stations);
        model.addAttribute("passengers", passengers);
        return "passengers";
    }
}
