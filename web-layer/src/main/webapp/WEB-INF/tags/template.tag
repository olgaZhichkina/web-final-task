<%@ tag import="org.springframework.security.core.Authentication" %>
<%@ tag import="org.springframework.security.core.context.SecurityContextHolder" %>
<%@ tag description="Simple wrapper tag" pageEncoding="utf-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>

<html>
<head>
    <link rel="stylesheet" href="<c:url value="/static/resources/css/style.css"/>"/>
    <link rel="stylesheet" href="<c:url value="/static/resources/css/twitter.css"/>"/>
    <script src="<c:url value="/static/resources/js/jquery-2.1.1.min.js"/>"></script>
    <script src="<c:url value="/static/resources/js/script.js"/>"></script>
</head>
<body>
<div id="header">
    <ul class="nav nav-pills">
        <li>
            <a href="search">Search</a>
        </li>
        <li>
            <a href="mytickets">My Tickets</a>
        </li>
        <li>
            <a href="purchaseinfo">Info</a>
        </li>
        <sec:authorize access="hasRole('ROLE_ADMIN')">
            <li>
                <a href="stations">Stations</a>
            </li>
            <li>
                <a href="trains">Trains</a>
            </li>
            <li>
                <a href="passengers">Passengers</a>
            </li>
        </sec:authorize>
        <li>
            <%
                Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
                String name = authentication.getName();
            %>
        </li>
        <li>
            <a href="<c:url value="j_spring_security_logout" />">Logout (<%= name %>)</a>
        </li>
    </ul>

</div>
<div id="content">
    <jsp:doBody/>
</div>
</body>
</html>