<%@ page contentType="text/html" pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <link rel="stylesheet" href="<c:url value="/static/resources/css/style.css"/>"/>
    <link rel="stylesheet" href="<c:url value="/static/resources/css/twitter.css"/>"/>
    <title>Login</title>
</head>
<body>
<h3 id="loginForm">Login Form</h3>

<div>
    <form class="form-horizontal" action="<c:url value='j_spring_security_check'/>" method="post">
        <c:if test="${not empty error}">
            <div class="alert alert-error">
                <h4>${error}</h4>
            </div>
        </c:if>
        <c:if test="${not empty success}">
            <div class="alert alert-success">
                <h4>${success}</h4>
            </div>
        </c:if>

        <div class="control-group">
            <label class="control-label" for="inputLogin">Login</label>

            <div class="controls">
                <input name="login" type="text" id="inputLogin" placeholder="Login">
            </div>
        </div>
        <div class="control-group">
            <label class="control-label" for="inputPassword">Password</label>

            <div class="controls">
                <input name="password" type="password" id="inputPassword" placeholder="Password">
            </div>
        </div>
        <div class="control-group">
            <div class="controls">
                <button type="submit" name="submit" class="btn">Sign in</button>
            </div>
        </div>
    </form>
    <form class="form-horizontal" action="registration" method="get">
        <div class="control-group">
            <div class="controls">
                <button type="submit" name="submit" class="btn">Register</button>
            </div>
        </div>
    </form>
</div>
</body>
</html>

