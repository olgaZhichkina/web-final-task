<%@ page contentType="text/html" pageEncoding="UTF-8" %>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<t:template>
    <h3>Stations administration</h3>

    <div id="stationsInfo">
        <c:if test="${not empty error}">
            <div class="alert alert-error">
                <h4>${error}</h4>
            </div>
        </c:if>
        <c:if test="${not empty success}">
            <div class="alert alert-success">
                <h4>${success}</h4>
            </div>
        </c:if>
        <table class="table table-striped">
            <tr>
                <th>Name</th>
            </tr>
            <c:forEach var="station" items="${stations}">
                <tr>
                    <td>${station.name}</td>
                </tr>
            </c:forEach>
        </table>
        <form class="form-horizontal" method="post" action="stations">
            <div class="control-group">
                <label class="control-label" for="inputStationName">Station Name</label>

                <div class="controls">
                    <input type="text" id="inputStationName" placeholder="Station Name" name="stationName">
                </div>
            </div>
            <div class="control-group">
                <div class="controls">
                    <button type="submit" class="btn" id="addStationButton">Add Station</button>
                </div>
            </div>
        </form>
    </div>
</t:template>

