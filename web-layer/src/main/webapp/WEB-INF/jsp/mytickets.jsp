<%@ page contentType="text/html" pageEncoding="UTF-8" %>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<t:template>
    <div>
        <c:choose>
            <c:when test="${not empty tickets}">
                <table id="myTickets" class="table">
                    <tr>
                        <th>Ticket #</th>
                        <th>Date</th>
                        <th>Train</th>
                        <th>From</th>
                        <th>Departure</th>
                        <th>To</th>
                        <th>Arrival</th>
                        <th>Purchasing date
                        <th>
                    </tr>
                    <c:forEach var="ticket" items="${tickets}">
                        <tr>
                            <td>${ticket.ticketNumber}</td>
                            <td>${ticket.dateOfTrip}</td>
                            <td>${ticket.trainName}</td>
                            <td>${ticket.fromStation}</td>
                            <td>${ticket.departureTime}</td>
                            <td>${ticket.toStation}</td>
                            <td>${ticket.arrivalTime}</td>
                            <td>${ticket.dateOfPurchase}</td>
                        </tr>
                    </c:forEach>
                </table>
            </c:when>
            <c:otherwise>
                <div class="alert alert-info">
                    <h4>You have no tickets</h4>
                </div>
            </c:otherwise>
        </c:choose>
    </div>
</t:template>
