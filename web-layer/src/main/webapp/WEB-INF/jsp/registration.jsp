<%@ page contentType="text/html" pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
<head>
    <link rel="stylesheet" href="<c:url value="/static/resources/css/style.css"/>"/>
    <link rel="stylesheet" href="<c:url value="/static/resources/css/twitter.css"/>"/>
    <title>Registration</title>
</head>
<body>
<h3 id="regForm">Registration Form</h3>
<div>
    <form name="registration-form" class="form-horizontal" action="login?register=1" method="post">
        <div class="control-group">
            <label class="control-label" for="inputFirstName">First Name</label>
            <div class="controls">
                <input type="text" id="inputFirstName" placeholder="First Name" name="firstName">
            </div>
        </div>
        <div class="control-group">
            <label class="control-label" for="inputLastName">Last Name</label>
            <div class="controls">
                <input type="text" id="inputLastName" placeholder="Last Name" name="lastName">
            </div>
        </div>
        <div class="control-group">
            <label class="control-label" for="inputBirthDate">Date Of Birth</label>
            <div class="controls">
                <input type="date" id="inputBirthDate" name="calendar">
            </div>
        </div>
        <div class="control-group">
            <label class="control-label" for="newLogin">Login</label>
            <div class="controls">
                <input type="text" id="newLogin" placeholder="Login" name="newLogin">
            </div>
        </div>
        <div class="control-group">
            <label class="control-label" for="newPassword">Password</label>
            <div class="controls">
                <input type="password" id="newPassword" placeholder="Password" name="newPassword">
            </div>
        </div>
        <div class="control-group">
            <div class="controls">
                <button type="submit" class="btn">Register</button>
            </div>
        </div>
    </form>
</div>
</body>
</html>
