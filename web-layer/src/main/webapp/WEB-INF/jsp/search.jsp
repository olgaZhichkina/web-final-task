<%@ page contentType="text/html" pageEncoding="UTF-8" %>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<t:template>
    <div>
        <form class="form-horizontal" method="post" action="search">
            <div class="control-group">
                <label class="control-label" for="inputFrom">From</label>

                <div class="controls">
                    <select name="departure" id="inputFrom">
                        <c:forEach var="station" items="${stations}">
                            <option ${departureStationId == station.id ? "selected" : ""}
                                    value="${station.id}">${station.name}</option>
                        </c:forEach>
                    </select>
                </div>
            </div>

            <div class="control-group">
                <label class="control-label" for="inputTo">To</label>

                <div class="controls">
                    <select name="arrival" id="inputTo">
                        <option ${arrivalStationId == "0" ? "selected" : ""}
                                value="0">"not selected"
                        </option>
                        <c:forEach var="station" items="${stations}">
                            <option ${arrivalStationId == station.id ? "selected" : ""}
                                    value="${station.id}">${station.name}</option>
                        </c:forEach>
                    </select>
                </div>
            </div>

            <div class="control-group">
                <label class="control-label" for="inputDate">Date</label>

                <div class="controls">
                    <input type="date" name="calendar" id="inputDate" value="${date}">
                </div>
            </div>

            <div class="control-group">
                <div class="controls">
                    <button type="submit" class="btn">Search</button>
                </div>
            </div>
        </form>
    </div>

    <div class="alert alert-error hide">
        <h4>Unfortunately, you could not buy this train ticket.
            Please, see <a href="purchaseinfo">Info</a> for more information</h4>
    </div>

    <div class="alert alert-success hide">
        <h4>You have successfully bought this ticket.
            Please, see <a href="mytickets">My Tickets</a> for more information</h4>
    </div>

    <div id="trains">
        <c:choose>
            <c:when test="${not empty lines}">
                <table class="table table-hover" id="trainOptions">
                    <tr>
                        <th>Train</th>
                        <th>Departure</th>
                        <th>Time</th>
                        <c:if test="${empty isInfo}">
                            <th>Arrival</th>
                            <th>Time</th>
                            <th>Number of seats</th>
                            <th style="display: none">departureId</th>
                            <th style="display: none">arrivalId</th>
                        </c:if>
                    </tr>
                    <c:forEach var="line" items="${lines}">
                        <tr>
                            <td>${line.train}</td>
                            <td>${line.departureStation}</td>
                            <td>${line.timeOfDeparture}</td>
                            <c:if test="${empty isInfo}">
                                <td>${line.arrivalStation}</td>
                                <td>${line.timeOfArrival}</td>
                                <td>${line.numberOfSeats}</td>
                                <td style="display: none" id="from">${line.departureId}</td>
                                <td style="display: none" id="to">${line.arrivalId}</td>
                            </c:if>
                        </tr>
                    </c:forEach>
                </table>
                <c:if test="${empty isInfo}">
                    <div class="control-group">
                        <div class="controls">
                            <button type="submit" class="btn" id="buyButton" disabled="disabled">Buy</button>
                        </div>
                    </div>
                </c:if>
            </c:when>
            <c:otherwise>
                <c:if test="${not empty searching}">
                    <div class="alert alert-info">
                        <h4>No options found for this route</h4>
                    </div>
                </c:if>
            </c:otherwise>
        </c:choose>
    </div>
</t:template>
