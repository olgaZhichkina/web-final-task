<%@ page contentType="text/html" pageEncoding="UTF-8" %>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<t:template>
    <h3>Trains administration</h3>

    <div id="trainsInfo">
        <c:if test="${not empty error}">
            <div class="alert alert-error">
                <h4>${error}</h4>
            </div>
        </c:if>
        <c:if test="${not empty success}">
            <div class="alert alert-success">
                <h4>${success}</h4>
            </div>
        </c:if>
        <table class="table table-striped">
            <tr>
                <th>Name</th>
                <th>Number of seats</th>
            </tr>
            <c:forEach var="train" items="${trains}">
                <tr>
                    <td>${train.name}</td>
                    <td>${train.numberOfSeats}</td>
                </tr>
            </c:forEach>
        </table>
        <form class="form-horizontal" method="post" action="trains">
            <div class="control-group">
                <label class="control-label" for="inputTrainName">Train Name</label>

                <div class="controls">
                    <input type="text" id="inputTrainName" placeholder="Train Name" name="trainName">
                </div>
            </div>
            <div class="control-group">
                <label class="control-label" for="inputNumOfSeats">Number Of Seats</label>

                <div class="controls">
                    <input type="text" id="inputNumOfSeats" placeholder="Number of seats" name="numOfSeats">
                </div>
            </div>
            <div class="control-group">
                <label class="control-label" for="stationOnRoute01">Station</label>

                <div class="controls">
                    <select name="stationOnRoute01" id="stationOnRoute01">
                        <c:forEach var="station" items="${stations}">
                            <option>${station.name}</option>
                        </c:forEach>
                    </select>
                </div>
            </div>
            <div class="control-group">
                <label class="control-label" for="inputTime01">Time</label>

                <div class="controls">
                    <input type="time" id="inputTime01" name="time01" value="00:00" min="00:00" max="23:59">
                </div>

            </div>
            <div class="control-group">
                <label class="control-label" for="stationOnRoute02">Station</label>

                <div class="controls">
                    <select name="stationOnRoute02" id="stationOnRoute02">
                        <c:forEach var="station" items="${stations}">
                            <option>${station.name}</option>
                        </c:forEach>
                    </select>
                </div>
            </div>
            <div class="control-group">
                <label class="control-label" for="inputTime02">Time</label>

                <div class="controls">
                    <input type="time" id="inputTime02" name="time02" value="00:00" min="00:00" max="23:59">
                </div>
            </div>
            <div class="control-group">
                <div class="controls">
                    <button type="button" class="btn" id="addLine" disabled="disabled">More Stations</button>
                </div>
            </div>
            <div class="control-group">
                <div class="controls">
                    <button type="submit" class="btn" id="addTrainButton">Add Train</button>
                </div>
            </div>
        </form>
    </div>
</t:template>
