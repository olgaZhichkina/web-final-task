<%@ page contentType="text/html" pageEncoding="UTF-8" %>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<t:template>
    <h3>Passengers administration</h3>

    <div id="passengersInfo">
        <form class="form-horizontal" method="post" action="passengers">
            <div class="control-group">
                <label class="control-label" for="train">Train</label>

                <div class="controls">
                    <select name="train" id="train">
                        <c:forEach var="train" items="${trains}">
                            <option ${trainId == train.id ? "selected" : ""}
                                    value="${train.id}">${train.name}</option>
                        </c:forEach>
                    </select>
                </div>
            </div>
            <div class="control-group">
                <label class="control-label" for="selectDate">Date</label>

                <div class="controls">
                    <input type="date" name="calendar" id="selectDate" value="${date}">
                </div>
            </div>
            <div class="control-group">
                <div class="controls">
                    <button type="submit" class="btn" id="showInfoButton">Show Information</button>
                </div>
            </div>
        </form>

        <c:if test="${not empty stations}">
            <h4>Stations</h4>
            <table class="table table-striped" id="stationsTable">
                <tr>
                    <th>Station Name</th>
                    <th>Time</th>
                </tr>
                <c:forEach var="station" items="${stations}">
                    <tr>
                        <td>${station.name}</td>
                        <td>${station.time}</td>
                    </tr>
                </c:forEach>
            </table>
        </c:if>

        <c:if test="${not empty passengers}">
            <h4>Passengers</h4>
            <table class="table table-striped" id="passengersTable">
                <tr>
                    <th>First Name</th>
                    <th>Last Name</th>
                    <th>Date Of Birth</th>
                </tr>
                <c:forEach var="passenger" items="${passengers}">
                    <tr>
                        <td>${passenger.firstName}</td>
                        <td>${passenger.lastName}</td>
                        <td>${passenger.dateOfBirth}</td>
                    </tr>
                </c:forEach>
            </table>
        </c:if>
    </div>
</t:template>
