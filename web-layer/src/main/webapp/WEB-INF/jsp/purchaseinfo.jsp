<%@ page contentType="text/html" pageEncoding="UTF-8" %>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<t:template>
    <p class="text" id="information">In order to find the appropriate train, please select the departure station
        and (optionally) arrival station and date. In order to buy the ticket, please select the train
        and press the Buy button. You can only buy one ticket per train if there are free seats
        and more than 10 minutes before the departure.</p>
</t:template>
