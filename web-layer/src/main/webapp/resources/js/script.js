var lastSelected;
var selected;

$(document).ready(function () {


    $("#trainOptions").find("tr:not(:first)").on("click", function () {
        $(".alert").hide();
        var selectedChild = event.target;
        selected = selectedChild.parentNode;
        selected.style.backgroundColor = "#dddddd";
        if (lastSelected != null) {
            lastSelected.style.backgroundColor = "#ffffff";
        }
        if (lastSelected == selected) {
            selected = null;
            $("#buyButton").attr('disabled','disabled');
        }
        lastSelected = selected;
        if (selected != null) {
            selected=$(this);
            $("#buyButton").removeAttr('disabled');
        }
    });

    $("#buyButton").on("click", function () {
        $(".alert").hide()
        $.ajax({
            url: "buy",
            type: "POST",
            data: {
                "from": selected.find("#from").text(),
                "to": selected.find("#to").text(),
                "calendar": $("#inputDate").val()
            }
        }).done(function(isBought) {
                if(isBought == "true")
                    $(".alert-success").show();
                else
                    $(".alert-error").show();
        });
        return false;
    });
});